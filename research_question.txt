# Research Question - Group 24

Members: Amogh Prabhakar,Venkata Sai Kumar Reddy Koppula,Haripriya Satheesh, Yogesh Savasere Ramachandra,Varun Srinivasamurthy.
Topic: Decision Tree and Random Forest Based Classification Model to Predict Diabetes. 

RQ: "How does the performance of a Random Forest Classifier compare to a Decision Tree Classifier in the prediction of Diabetes?"

Context: Prediction of Diabetes patients.
	
Population: Diabetes patients.

Intervention: Machine learning models used to classify problems (Random Forest and Decision tree).

Comparison: The accuracy of Random Forest Classifier vs Decision Tree Classifier.

Outcome: Accurate prediction of Random Forest Classification.



